object CoreConfig {
    const val basUrl = """ "https://api.spotify.com/v1/"  """
    const val REDIRECT_URI = """ "https://www.mydigipay.com/" """
    const val CLIENT_ID = """ "54e90139749f4bd092a0e91f7b047f42" """
}

object Versions {

    const val gradle_plugin_version = "3.4.2"
    const val kotlin_version = "1.3.31"
    const val coroutine_version = "1.3.0-RC"
    const val retrofit_version = "2.6.0"
    const val okhttp_version = "3.10.0"
    const val android_x_version = "1.0.2"
    const val app_compat_version = "1.1.0-rc01"
    const val material_version = "1.1.0-alpha08"
    const val navigation_version = "2.1.0-beta02"
    const val constraint_layout_version = "2.0.0-beta3"
    const val lifecycle_version = "2.0.0-beta01"
    const val lifecycle_view_model_version = "2.2.0-alpha02"
    const val koin_version = "2.0.1"
    const val glide_version = "4.9.0"
    const val paging_version = "2.1.0"
    const val spotify_version = "1.1.0"



}


object Dependencies {
    //kotlin
    val kotlin = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin_version}"

    //coroutines
    val coroutine_core = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.coroutine_version}"
    val coroutine_android = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutine_version}"
    val coroutine_debug = "org.jetbrains.kotlinx:kotlinx-coroutines-debug:${Versions.coroutine_version}"
    val coroutine_test = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.coroutine_version}"

    //retrofit
    val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit_version}"
    val retrofit_gson_adapter = "com.squareup.retrofit2:converter-gson:${Versions.retrofit_version}"
    val retrofit_mock = "com.squareup.retrofit2:retrofit-mock:${Versions.retrofit_version}"
    val okHttpLogger = "com.squareup.okhttp3:logging-interceptor:${Versions.okhttp_version}"

    //glide
    val glide = "com.github.bumptech.glide:glide:${Versions.glide_version}"
    val glideCompiler = "com.github.bumptech.glide:compiler:${Versions.glide_version}"

    //spotify auth
    val spotifyAuth = "com.spotify.android:auth:${Versions.spotify_version}"
}

object AndoridDepencencies {

    //android x
    val androidxCore = "androidx.core:core-ktx:${Versions.android_x_version}"
    val appCompat = "androidx.appcompat:appcompat:${Versions.app_compat_version}"
    val material = "com.google.android.material:material:${Versions.material_version}"
    val constraintlayout = "androidx.constraintlayout:constraintlayout:${Versions.constraint_layout_version}"

    val lifecycle = "androidx.lifecycle:lifecycle-extensions:${Versions.lifecycle_version}"
    val lifecycle_test = "androidx.arch.core:core-testing:${Versions.lifecycle_version}"
    val lifecycle_view_model = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle_view_model_version}"

    val navigation = "androidx.navigation:navigation-runtime:${Versions.navigation_version}"
    val navigationKtx = "androidx.navigation:navigation-runtime-ktx:${Versions.navigation_version}"
    val navigationFragment = "androidx.navigation:navigation-fragment:${Versions.navigation_version}"
    val navigationFragmentKtx = "androidx.navigation:navigation-fragment-ktx:${Versions.navigation_version}"
    val navigationUi = "androidx.navigation:navigation-ui:${Versions.navigation_version}"
    val navigationUiKtx = "androidx.navigation:navigation-ui-ktx:${Versions.navigation_version}"

    val paging = "androidx.paging:paging-runtime:${Versions.paging_version}"
    val paging_runtime = "androidx.paging:paging-common:${Versions.paging_version}"

    //koin
    val koin = "org.koin:koin-android:${Versions.koin_version}"
    val koinViewmodel = "org.koin:koin-android-viewmodel:${Versions.koin_version}"
    val koinScope = "org.koin:koin-android-scope:${Versions.koin_version}"

}

object Plugins {
    val kotlin_gradle_plugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin_version}"
    val android_gradle_plugin = "com.android.tools.build:gradle:${Versions.gradle_plugin_version}"
    val navigation_safe_arg_gradle_plugin = "androidx.navigation:navigation-safe-args-gradle-plugin:${Versions.navigation_version}"

}
