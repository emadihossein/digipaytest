plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-android-extensions")
    id("kotlin-kapt")
    id("androidx.navigation.safeargs")
}

android {
    compileSdkVersion(28)
    defaultConfig {
        applicationId = "com.digipaytest.app"

        minSdkVersion(21)
        targetSdkVersion(28)
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        buildConfigField("String", "base_Url", CoreConfig.basUrl)
        buildConfigField("String", "CLIENT_ID", CoreConfig.CLIENT_ID)
        buildConfigField("String", "REDIRECT_URI", CoreConfig.REDIRECT_URI)
        androidExtensions {
            isExperimental = true
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        setTargetCompatibility(JavaVersion.VERSION_1_8)
    }


    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }

    androidExtensions {
        isExperimental = true
    }
}


dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    implementation(Dependencies.coroutine_core)
    implementation(Dependencies.coroutine_android)
    implementation(Dependencies.coroutine_debug)


    implementation(AndoridDepencencies.appCompat)
    implementation(AndoridDepencencies.material)
    implementation(AndoridDepencencies.constraintlayout)

    // ViewModel and LiveData
    implementation(AndoridDepencencies.lifecycle)
    implementation("androidx.appcompat:appcompat:1.1.0")
    implementation("androidx.constraintlayout:constraintlayout:1.1.3")
    testImplementation(AndoridDepencencies.lifecycle_test)
    implementation(AndoridDepencencies.lifecycle_view_model)
    implementation(AndoridDepencencies.koin)
    implementation(AndoridDepencencies.koinScope)
    implementation(AndoridDepencencies.koinViewmodel)
    implementation(AndoridDepencencies.androidxCore)


    //navigation
    implementation(AndoridDepencencies.navigation)
    implementation(AndoridDepencencies.navigationKtx)
    implementation(AndoridDepencencies.navigationFragment)
    implementation(AndoridDepencencies.navigationFragmentKtx)
    implementation(AndoridDepencencies.navigationUi)
    implementation(AndoridDepencencies.navigationUiKtx)


    //pating
    implementation(AndoridDepencencies.paging)
    implementation(AndoridDepencencies.paging_runtime)

    //retrofit
    implementation(Dependencies.retrofit)
    implementation(Dependencies.retrofit_gson_adapter)
    implementation(Dependencies.retrofit_mock)
    implementation(Dependencies.okHttpLogger)

    //image load
    implementation(Dependencies.glide)
    annotationProcessor(Dependencies.glideCompiler)

    //spotify login
    implementation(Dependencies.spotifyAuth)


}
