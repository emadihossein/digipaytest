package com.digipaytest.app.features.oAuth2Feature

import com.moshaverOnline.app.core.base.BaseResponseModel
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST


interface  RefreshTokenNetwork{
@POST("/Auth/RefreshToken")
suspend fun refreshToken(@Body refreshRequest:RefreshTokenRequest): Response<BaseResponseModel<RefreshTokenResponse>>
}


