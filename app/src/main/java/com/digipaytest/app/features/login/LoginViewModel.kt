package com.digipaytest.app.features.login

import androidx.lifecycle.LiveData
import com.digipaytest.app.core.base.BaseViewModel
import com.digipaytest.app.core.util.livedata.SingleLiveEvent

class LoginViewModel(private val splashRepository: LoginRepository): BaseViewModel() {
    private val tokenSavedMutableLiveData: SingleLiveEvent<Boolean> = SingleLiveEvent()
    val tokenSavedLiveData: LiveData<Boolean>
        get() = tokenSavedMutableLiveData

    private val tokenErrorMutableLiveData: SingleLiveEvent<String> = SingleLiveEvent()
    val tokenErrorLiveData: LiveData<String>
        get() = tokenErrorMutableLiveData

    private val requestLoginMutableLiveData: SingleLiveEvent<Boolean> = SingleLiveEvent()
    val requestLoginLiveData: LiveData<Boolean>
        get() = requestLoginMutableLiveData

    fun newToken(string: String){
        progress.postValue(false)
        splashRepository.newToken(string)
        tokenSavedMutableLiveData.postValue(true)
    }
    fun newTokenError(string: String) {
        progress.postValue(false)
        tokenErrorMutableLiveData.postValue(string)
    }

    fun requestLogin() {
        progress.postValue(true)
        requestLoginMutableLiveData.postValue(true)
    }
}