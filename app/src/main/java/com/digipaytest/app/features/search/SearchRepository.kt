package com.digipaytest.app.features.search

import com.digipaytest.app.core.base.BaseRepository
import com.digipaytest.app.core.exception.Failure
import com.digipaytest.app.platform.NetworkHandler
import com.fernandocejas.sample.core.functional.Either

interface SearchRepository{
    suspend fun search(searchRequestModel: SearchRequestModel): Either<Failure, SearchArtistsResponseModel>
}

class SearchRepositoryImp(private val searchNetwork: SearchNetwork, networkHandler: NetworkHandler): SearchRepository, BaseRepository(networkHandler){
    override suspend fun search(searchRequestModel: SearchRequestModel): Either<Failure, SearchArtistsResponseModel> {
        return request({searchNetwork.search(searchRequestModel.query, searchRequestModel.type.toString(), searchRequestModel.limit, searchRequestModel.offset)},{it}, SearchArtistsResponseModel.default())
    }

}