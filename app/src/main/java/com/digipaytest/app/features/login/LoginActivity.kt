package com.digipaytest.app.features.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import com.digipaytest.app.BuildConfig
import com.digipaytest.app.R
import com.digipaytest.app.features.search.SearchActivity
import com.digipaytest.app.platform.CustomBaseActivity
import com.spotify.sdk.android.authentication.AuthenticationClient
import com.spotify.sdk.android.authentication.AuthenticationRequest
import com.spotify.sdk.android.authentication.AuthenticationResponse
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : CustomBaseActivity<LoginViewModel>(LoginViewModel::class) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        setupClickListeners()
        viewModel.tokenErrorLiveData.observe(this, Observer { t ->
            Toast.makeText(this, t, Toast.LENGTH_LONG).show()
        })
        viewModel.tokenSavedLiveData.observe(this, Observer { t ->
            if(t) goToMainActivity()
        })
        viewModel.requestLoginLiveData.observe(this, Observer { t ->
            loginRequestFromSdk(LOGIN_REQUEST_CODE)
        })

        viewModel.progress.observe(this, Observer { t ->
            if(t){
                progressBar.visibility = View.VISIBLE
                btnLogin.isEnabled = false
            }
            else{
                progressBar.visibility = View.GONE
                btnLogin.isEnabled = true

            }
        })

    }

    private fun goToMainActivity() {
        startActivity(Intent(this, SearchActivity::class.java).apply{
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        })

    }

    private fun loginRequestFromSdk(loginRequestCode: Int) {
        val builder: AuthenticationRequest.Builder =
            AuthenticationRequest.Builder(
                BuildConfig.CLIENT_ID,
                AuthenticationResponse.Type.TOKEN,
                BuildConfig.REDIRECT_URI
            )

        builder.setScopes(arrayOf("streaming"))
        val request: AuthenticationRequest = builder.build()

        AuthenticationClient.openLoginActivity(this, loginRequestCode, request)
    }

    private fun setupClickListeners() {
        btnLogin.setOnClickListener {
            viewModel.requestLogin()
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)

        if (requestCode == LOGIN_REQUEST_CODE) {
            val response = AuthenticationClient.getResponse(resultCode, intent)
            when (response.type) {
                AuthenticationResponse.Type.TOKEN -> {viewModel.newToken(response.accessToken)}
                AuthenticationResponse.Type.ERROR -> {viewModel.newTokenError(response.error)}
                else -> {viewModel.newTokenError(getString(R.string.login_not_complete))}
            }
        }
    }

    companion object {
        private const val LOGIN_REQUEST_CODE = 1337
    }
}
