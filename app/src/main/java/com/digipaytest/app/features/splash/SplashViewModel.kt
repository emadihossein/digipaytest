package com.digipaytest.app.features.splash

import androidx.lifecycle.LiveData
import com.digipaytest.app.core.base.BaseViewModel
import com.digipaytest.app.core.util.livedata.SingleLiveEvent

class SplashViewModel(private val splashRepository: SplashRepository): BaseViewModel() {
    private val checkTokenExistMutableLiveData: SingleLiveEvent<Boolean> = SingleLiveEvent()
    val checkTokenExistLiveData: LiveData<Boolean>
        get() = checkTokenExistMutableLiveData


    fun init() {
        if (splashRepository.checkTokenExist()) {
            checkTokenExistMutableLiveData.postValue(true)
            return
        }
        checkTokenExistMutableLiveData.postValue(false)
    }

}