package com.digipaytest.app.features.search

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

data class SearchArtistsResponseModel(var items: List<Item?>){
    companion object {
        fun default(): SearchArtistsResponseModel = SearchArtistsResponseModel(listOf())
    }
}
@Parcelize
data class Item(var images: List<Image>?, var name:String?, var genres: List<String>?, var popularity: Int?):
    Parcelable
@Parcelize
data class Image(var url: String?): Parcelable
data class SearchRequestModel(var query: String, val type: SearchRequestType, val limit: Int, val offset: Int){
    companion object{
        const val defaultLimit = 10
    }
}


enum class SearchRequestType{
    artist
}