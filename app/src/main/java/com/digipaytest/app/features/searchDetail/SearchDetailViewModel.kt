package com.digipaytest.app.features.searchDetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.digipaytest.app.core.base.BaseViewModel
import com.digipaytest.app.features.search.Item

class SearchDetailViewModel: BaseViewModel() {
    private var itemMutableLiveData: MutableLiveData<Item> = MutableLiveData()
    val itemLiveData: LiveData<Item>
        get() = itemMutableLiveData

    fun init(item: Item) {
        itemMutableLiveData.postValue(item)
    }
}