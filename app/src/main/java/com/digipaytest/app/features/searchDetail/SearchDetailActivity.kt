package com.digipaytest.app.features.searchDetail

import android.os.Bundle
import androidx.lifecycle.Observer
import com.digipaytest.app.R
import com.digipaytest.app.core.extentions.loadFromUrl
import com.digipaytest.app.features.search.Item
import com.digipaytest.app.platform.CustomBaseActivity
import kotlinx.android.synthetic.main.activity_search_detail.*

class SearchDetailActivity : CustomBaseActivity<SearchDetailViewModel>(SearchDetailViewModel::class) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_detail)
    }

    override fun onStart() {
        super.onStart()
        viewModel.itemLiveData.observe(this, Observer {
            t -> initView(t)
        })
    }

    private fun initView(t: Item?) {
        t?.let {
            txtName.text = it.name
            txtGenre.text = it.genres?.joinToString()
            txtPopularity.text = "popularity : ${it.popularity}/100"
            it.images?.let {list ->
                if(!list.isEmpty())
                    img.loadFromUrl(list.first()?.url)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        val item = intent.getParcelableExtra<Item>("extra_item")
        item?.let {viewModel.init(it) }

    }
}
