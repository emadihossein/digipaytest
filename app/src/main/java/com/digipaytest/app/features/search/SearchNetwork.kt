package com.digipaytest.app.features.search

import com.moshaverOnline.app.core.base.BaseResponseModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface SearchNetwork {

    @GET("search")
    suspend fun search(@Query("q") query: String, @Query ("type") type:String, @Query ("limit") limit:Int, @Query ("offset") offset:Int): Response<BaseResponseModel<SearchArtistsResponseModel>>

}