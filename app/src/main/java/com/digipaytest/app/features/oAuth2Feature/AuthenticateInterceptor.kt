package com.digipaytest.app.features.oAuth2Feature

import okhttp3.Interceptor
import okhttp3.Response

class AuthenticateInterceptor(private val oauthRepository: OauthRepository) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val builder = chain?.request()?.newBuilder()
        builder?.addHeader("Authorization", "Bearer ${oauthRepository.getAccessToken()}")
        val newRequest = builder?.build()
        return chain?.proceed(newRequest)!!
    }
}