package com.digipaytest.app.features.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.digipaytest.app.core.base.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SearchViewModel(private val searchRepository: SearchRepository): BaseViewModel() {

    private var loading: Boolean = false
    private var offset: Int = 1 // for handle paging list

    private var refreshSearchResultMutableLiveData: MutableLiveData<MutableList<Item>> = MutableLiveData()
    val refreshSearchLiveData: LiveData<MutableList<Item>>
        get() = refreshSearchResultMutableLiveData

    private var searchResultMutableLiveData: MutableLiveData<MutableList<Item>> = MutableLiveData()
    val searchResultLiveData: LiveData<MutableList<Item>>
        get() = searchResultMutableLiveData


    fun newSearch(string: String) {
        handleProgress(true)
        viewModelScope.launch(Dispatchers.IO){
            val searchModel = SearchRequestModel(string, SearchRequestType.artist, SearchRequestModel.defaultLimit, 1)
            val data = searchRepository.search(searchModel)
            data.either(::handleFailure, ::newSearchDone)
        }
    }

    fun search(string: String) {
        if (!loading) {
            handleProgress(true)
            viewModelScope.launch(Dispatchers.IO) {
                val searchModel = SearchRequestModel(
                    string,
                    SearchRequestType.artist,
                    SearchRequestModel.defaultLimit,
                    offset
                )
                val data = searchRepository.search(searchModel)
                data.either(::handleFailure.also {
                    //if error occured
                    loading = false
                }, ::searchDone)
            }
            loading = true
        }
    }

    private fun searchDone(searchArtistsResponseModel: SearchArtistsResponseModel) {
        handleProgress(false)
        searchResultMutableLiveData.postValue(searchArtistsResponseModel.items as MutableList<Item>?)
        loading = false
        plusOffset()
    }


    private fun plusOffset() {
        offset += SearchRequestModel.defaultLimit

    }

    private fun newSearchDone(searchArtistsResponseModel: SearchArtistsResponseModel) {
        handleProgress(false)
        refreshSearchResultMutableLiveData.postValue(searchArtistsResponseModel.items as MutableList<Item>?)
        plusOffset()
    }

}