package com.digipaytest.app.features.oAuth2Feature

import android.content.SharedPreferences
import com.digipaytest.app.core.extentions.getPref
import com.digipaytest.app.core.extentions.setPref
import com.moshaverOnline.app.core.util.ACCESS_TOKEN_KEY
import com.moshaverOnline.app.core.util.REFRESH_TOKEN_KEY

interface OathPrefensManager {
    fun isAccessTokenExist(): Boolean
    fun isRefreshTokenExist(): Boolean
    fun getSavedAccessToken(): String
    fun SaveAccessToken(accessToken: String)
    fun getSavedRefreshToken(): String
    fun SavedRefreshToken(accessToken: String)

}



class OathPrefensManagerImp(private val  prefs: SharedPreferences):OathPrefensManager   {
    override fun isRefreshTokenExist(): Boolean {
        return getSavedRefreshToken()!= ""
    }

    override fun isAccessTokenExist(): Boolean {
       return getSavedAccessToken()!= ""
    }

    override fun getSavedAccessToken(): String {
       return  prefs.getPref(ACCESS_TOKEN_KEY)?:""
    }

    override fun SaveAccessToken(accessToken: String) {
        prefs.setPref(ACCESS_TOKEN_KEY,accessToken)
    }

    override fun getSavedRefreshToken(): String {
        return  prefs.getPref(REFRESH_TOKEN_KEY)?:""
    }

    override fun SavedRefreshToken(accessToken: String) {
        prefs.setPref(REFRESH_TOKEN_KEY,prefs)
    }

}