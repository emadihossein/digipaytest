package com.digipaytest.app.features.oAuth2Feature




data class  RefreshTokenResponse(val RefreshToken:String, val AccessToken: String){ companion object{
    fun default(): RefreshTokenResponse = RefreshTokenResponse("","")
}}
data class  RefreshTokenRequest(val refreshToken:String)