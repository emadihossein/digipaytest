package com.digipaytest.app.features.splash

import android.content.SharedPreferences
import com.digipaytest.app.core.extentions.getPref
import com.moshaverOnline.app.core.util.ACCESS_TOKEN_KEY

interface SplashSharedPref {
    fun isAccessTokenExist(): Boolean
    fun getSavedAccessToken(): String
}


class SplashSharedPrefImp(private val  prefs: SharedPreferences): SplashSharedPref   {

    override fun isAccessTokenExist(): Boolean {
        return getSavedAccessToken()!= ""
    }
    override fun getSavedAccessToken(): String {
        return  prefs.getPref(ACCESS_TOKEN_KEY)?:""
    }


}