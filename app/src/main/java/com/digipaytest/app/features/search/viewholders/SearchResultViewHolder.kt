package com.digipaytest.app.features.search.viewholders

import android.view.View
import android.view.ViewGroup
import com.digipaytest.app.core.base.BaseViewHolder
import com.digipaytest.app.features.search.Item
import kotlinx.android.synthetic.main.item_search_result.view.*

class SearchResultViewHolder(private val view: View, val parent: ViewGroup) : BaseViewHolder<Item>(view, parent) {
    override fun bind(item: Item, clickListener: (Any) -> Unit) {
        view.txtName.text = item.name
        itemView.setOnClickListener { clickListener.invoke(item) }
    }
}

class ProgressBarViewHolder(view: View, val parent: ViewGroup) : BaseViewHolder<Item?>(view, parent) {
    override fun bind(item: Item?, clickListener: (Any) -> Unit) {}
}