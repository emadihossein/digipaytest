package com.digipaytest.app.features.splash

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import com.digipaytest.app.R
import com.digipaytest.app.features.login.LoginActivity
import com.digipaytest.app.features.search.SearchActivity
import com.digipaytest.app.platform.CustomBaseActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class SplashActivity : CustomBaseActivity<SplashViewModel>(SplashViewModel::class), CoroutineScope {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        viewModel.checkTokenExistLiveData.observe(this, Observer {
            launch {
                delay(3000)
                if(it) goToMainActivity()
                else goToLoginActivity()
            }

        })
    }

    private fun goToLoginActivity() {
        startActivity(Intent(this, LoginActivity::class.java).apply{
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        }

        )
    }

    private fun goToMainActivity() {
        startActivity(Intent(this, SearchActivity::class.java).apply{
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        })
    }

    override fun onStart() {
        super.onStart()
        viewModel.init()
    }

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main
}
