package com.digipaytest.app.features.login

import android.content.SharedPreferences
import com.digipaytest.app.core.extentions.getPref
import com.digipaytest.app.core.extentions.setPref
import com.moshaverOnline.app.core.util.ACCESS_TOKEN_KEY

interface LoginSharedPref {
    fun isAccessTokenExist(): Boolean
    fun getSavedAccessToken(): String
    fun saveAccessToken(accessToken: String)

}

class LoginSharedPrefImp(private val  prefs: SharedPreferences): LoginSharedPref   {

    override fun isAccessTokenExist(): Boolean {
        return getSavedAccessToken()!= ""
    }
    override fun getSavedAccessToken(): String {
        return  prefs.getPref(ACCESS_TOKEN_KEY)?:""
    }
    override fun saveAccessToken(accessToken: String) {
        prefs.setPref(ACCESS_TOKEN_KEY,accessToken)
    }
}