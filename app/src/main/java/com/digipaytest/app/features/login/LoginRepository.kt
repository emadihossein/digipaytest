package com.digipaytest.app.features.login

interface LoginRepository {
    fun newToken(token: String)
}

class LoginRepositoryImp(private val sharedPref: LoginSharedPref): LoginRepository{
    override fun newToken(token: String) {
        sharedPref.saveAccessToken(token)
    }

}