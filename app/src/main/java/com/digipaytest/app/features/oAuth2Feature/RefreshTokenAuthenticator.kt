package com.digipaytest.app.features.oAuth2Feature

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route

// for change  access token using refresh token when server return 401 error
class RefreshTokenAuthenticator(private val refreshTokenRepository: OauthRepository) : Authenticator {
    override fun authenticate(route: Route?, response: Response): Request? {
        return if (refreshTokenRepository.hasRefreshToken()) {
            runBlocking (Dispatchers.IO){

                response?.request()?.newBuilder()
                    ?.header("Authorization", "Bearer ${refreshTokenRepository.getAccessToken()}")
                    ?.build()
            }

        } else response?.request()
    }



}