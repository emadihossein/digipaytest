package com.digipaytest.app.features.search

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.digipaytest.app.R
import com.digipaytest.app.core.util.EndlessScrollListener
import com.digipaytest.app.features.search.adapter.SearchAdapter
import com.digipaytest.app.features.searchDetail.SearchDetailActivity
import com.digipaytest.app.platform.CustomBaseActivity
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class SearchActivity : CustomBaseActivity<SearchViewModel>(SearchViewModel::class), CoroutineScope {

    var job : Job? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        setupToolbar()
        etSearch.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if(s?.toString()?.trim() == "") return

                job = launch {
                    delay(1500)
                    viewModel.newSearch(s.toString())

                }
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                job?.cancel()
            }

        })

        val linearLayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)

        rcSearchResult.layoutManager = linearLayoutManager
        val adapter = SearchAdapter()

        adapter.clickListener = { it -> goToArtistPage(it)}


        val scrollListener = object : EndlessScrollListener(linearLayoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                viewModel.search(etSearch.text?.trim().toString())
            }
        }
        // adds the scroll listener to RecyclerView
        rcSearchResult.addOnScrollListener(scrollListener)
        rcSearchResult.adapter = adapter
        viewModel.refreshSearchLiveData.observe(this, Observer {
            scrollListener.resetState()
            adapter.swapData(it)
        })

        viewModel.searchResultLiveData.observe(this, Observer {
            adapter.appendItem(it)
        })
        viewModel.progress.observe(this, Observer {
            if(it) progressBar.visibility = View.VISIBLE
            else progressBar.visibility = View.GONE
        })
    }

    private fun setupToolbar() {
        center_txt.text = getString(R.string.search)
    }

    private fun goToArtistPage(item: Item) {
        startActivity(Intent(this, SearchDetailActivity::class.java)
            .apply { putExtra("extra_item", item) })

    }


    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main
}
