package com.digipaytest.app.features.search.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.digipaytest.app.R
import com.digipaytest.app.core.base.BaseViewHolder
import com.digipaytest.app.features.search.Item
import com.digipaytest.app.features.search.SearchArtistsResponseModel
import com.digipaytest.app.features.search.viewholders.ProgressBarViewHolder
import com.digipaytest.app.features.search.viewholders.SearchResultViewHolder

class SearchAdapter() : RecyclerView.Adapter<BaseViewHolder<*>>() {
    internal var clickListener: (Item) -> Unit = { _ -> }
    private var onLoadMoreListener: OnLoadMoreListener? = null
    private var data: MutableList<Item?> = ArrayList()


    fun swapData(newData: List<Item>) { // check this
        data.clear()
        data.addAll(newData)
        notifyDataSetChanged()
    }
    fun appendItem(data: List<Item>) {
        if (this.data.size == 0) {
            swapData(data)
        } else {
            this.data.addAll(data)
            notifyItemInserted(this.data.size - 1)
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        return when (viewType) {
            TYPE_VIEW -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_search_result, parent, false)
                SearchResultViewHolder(view, parent)
            }
            TYPE_PROGRESS -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_progress_bar, parent, false)
                ProgressBarViewHolder(view, parent)
            }
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }
    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        val element = data[position]
        when(holder){
            is ProgressBarViewHolder -> holder.bind(null, clickListener as (Any) -> Unit)
            is SearchResultViewHolder -> holder.bind(element as Item, clickListener as (Any) -> Unit)
            else -> throw IllegalArgumentException()
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (data[position] != null) TYPE_VIEW else TYPE_PROGRESS
    }


    companion object {
        private const val TYPE_VIEW = 1
        private const val TYPE_PROGRESS = 0
    }

    fun setOnLoadMoreListener(onLoadMoreListener: OnLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener
    }
    interface OnLoadMoreListener {
        fun onLoadMore()
    }
}
