package com.digipaytest.app.features.splash

interface SplashRepository {
    fun checkTokenExist(): Boolean
}

class SplashRepositoryImp(private val sharedPref: SplashSharedPref): SplashRepository{
    override fun checkTokenExist(): Boolean {
        if (sharedPref.isAccessTokenExist()) {
            return true
        }
        return false
    }

}
