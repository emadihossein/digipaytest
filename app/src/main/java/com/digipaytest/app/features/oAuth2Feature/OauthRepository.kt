package com.digipaytest.app.features.oAuth2Feature

import com.digipaytest.app.core.base.BaseRepository
import com.digipaytest.app.core.exception.Failure
import com.digipaytest.app.platform.NetworkHandler
import com.fernandocejas.sample.core.functional.Either

interface OauthRepository{
fun getAccessToken():String
fun hasRefreshToken():Boolean
fun saveNewToken(token:String)

}

class  OauthRepositoryImp (private val ouathPrefensManager: OathPrefensManager,networkHandler: NetworkHandler):
    BaseRepository(networkHandler),OauthRepository{
    override fun saveNewToken(token: String) {
        ouathPrefensManager.SaveAccessToken(token)
    }

    override fun getAccessToken(): String {
        return  ouathPrefensManager.getSavedAccessToken()
    }



    override fun hasRefreshToken(): Boolean {
        return  ouathPrefensManager.isAccessTokenExist()
    }
}