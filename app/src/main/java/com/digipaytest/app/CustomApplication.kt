package com.digipaytest.app

import android.app.Application
import com.digipaytest.app.core.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.PrintLogger

class CustomApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            PrintLogger()
            androidContext(this@CustomApplication)
            modules(
                listOf(
                    datamodule, networkmodule, splashModule, loginModule, searchModule, searchDetailModule
                )
            )
        }
    }

}