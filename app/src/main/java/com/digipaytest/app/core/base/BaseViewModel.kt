package com.digipaytest.app.core.base

import androidx.lifecycle.ViewModel
import com.digipaytest.app.core.exception.Failure
import com.digipaytest.app.core.util.livedata.SingleLiveEvent

abstract class BaseViewModel : ViewModel() {

    var failure: SingleLiveEvent<Failure> = SingleLiveEvent()
    var progress: SingleLiveEvent<Boolean> = SingleLiveEvent()

    protected fun handleFailure(failure: Failure) {
        this.failure.postValue(failure)
        handleProgress(false)
    }

    protected fun handleProgress(boolean: Boolean) {
        this.progress.postValue(boolean)
    }

}