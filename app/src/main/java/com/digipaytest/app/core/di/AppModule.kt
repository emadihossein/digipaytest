package com.digipaytest.app.core.di



import com.digipaytest.app.features.login.*
import com.digipaytest.app.features.search.SearchNetwork
import com.digipaytest.app.features.search.SearchRepository
import com.digipaytest.app.features.search.SearchRepositoryImp
import com.digipaytest.app.features.search.SearchViewModel
import com.digipaytest.app.features.searchDetail.SearchDetailActivity
import com.digipaytest.app.features.searchDetail.SearchDetailViewModel
import com.digipaytest.app.features.splash.*
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module



val splashModule = module {
    single <SplashSharedPref>{ SplashSharedPrefImp(get(named(DEFAULT_PREF))) }
    single<SplashRepository>{SplashRepositoryImp(get())}
    viewModel { SplashViewModel(get()) }
}

val loginModule = module {
    single <LoginSharedPref>{ LoginSharedPrefImp(get(named(DEFAULT_PREF))) }
    single<LoginRepository>{LoginRepositoryImp(get())}
    viewModel { LoginViewModel(get()) }
}

val searchModule = module {

        single<SearchRepository> { SearchRepositoryImp(createNetwork(SearchNetwork::class.java,get(named(AUTHENTICATED_RETROFIT))),get()) }
        viewModel { SearchViewModel(get()) }
}

val searchDetailModule = module {
    viewModel { SearchDetailViewModel() }
}