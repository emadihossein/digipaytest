package com.digipaytest.app.core.extentions

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.digipaytest.app.R

fun ImageView.loadFromUrl(url: String?, placeHolder: Int = R.mipmap.ic_launcher_round) {
    url?.let {
        Glide.with(this.context)
            .load(url)
            .placeholder(placeHolder)
            .error(R.mipmap.ic_launcher)
            .into(this)
    }

}


