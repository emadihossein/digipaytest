package com.digipaytest.app.core.di


import com.digipaytest.app.BuildConfig
import com.digipaytest.app.features.oAuth2Feature.*
import com.digipaytest.app.platform.NetworkHandler
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.Authenticator
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

const val AUTHENTICATED_CLIENT = "AUTHENTICATED_CLIENT"
const val AUTHENTICATED_RETROFIT = "AUTHENTICATED_RETROFIT"
const val REFRESH_TOKEN_AUTHENTICATOR = "REFRESH_TOKEN_AUTHENTICATOR"
const val JWT_TOKEN_INTERCEPTOR = "JWT_TOKEN_INTERCEPTOR"


val networkmodule = module {



    single { provideGson() }
    single{ NetworkHandler(get()) }

    //for mock server impelementation



    // for oauth 2 implementation
    single <OathPrefensManager>{OathPrefensManagerImp(get(named(DEFAULT_PREF)))}
    single  (named(JWT_TOKEN_INTERCEPTOR)){ AuthenticateInterceptor(get()) }
    single <OauthRepository>{ OauthRepositoryImp(get(),get()) }

    single (named(REFRESH_TOKEN_AUTHENTICATOR)) { RefreshTokenAuthenticator(get()) }
    single (named(AUTHENTICATED_CLIENT)){ provideAuthenticatedOkhttpClient(get(named(JWT_TOKEN_INTERCEPTOR))) }
    single(named(AUTHENTICATED_RETROFIT)) { provideAuthenticatedRetrofitModule(get(named(AUTHENTICATED_CLIENT)), get()) }

}


inline fun<reified T> createNetwork(service: Class<T> ,retrofit: Retrofit): T = retrofit.create( T::class.java)


fun provideDefaultOkhttpClient(): OkHttpClient {
    return OkHttpClient.Builder()
        .addInterceptor(HttpLoggingInterceptor().also { it.level = HttpLoggingInterceptor.Level.BODY })

        .build()
}



fun provideAuthenticatedRetrofitModule(client: OkHttpClient, gosn: Gson): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.base_Url)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create(gosn))
        .build()
}



fun provideAuthenticatedOkhttpClient(authenticateInterceptor: Interceptor): OkHttpClient {
    return OkHttpClient.Builder()
        .addInterceptor(authenticateInterceptor)
        .addInterceptor(HttpLoggingInterceptor().also { it.level = HttpLoggingInterceptor.Level.BODY })
        .build()
}







fun provideAnonymousRetrofitModule(client: OkHttpClient, gosn: Gson): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.base_Url)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create(gosn))
        .build()
}

fun provideGson(): Gson {


    return GsonBuilder().serializeNulls()
        .create()
}

