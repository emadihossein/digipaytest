package com.digipaytest.app.core.di

import android.preference.PreferenceManager
import org.koin.core.qualifier.named
import org.koin.dsl.module

const val DEFAULT_PREF = "DEFAULT_PREF"
    const val CUSTOM_PREF = "DEFAULT_PREF"
val datamodule = module {
    single (named(DEFAULT_PREF)){ PreferenceManager.getDefaultSharedPreferences(get()) }
}
