package com.digipaytest.app.core.base

import android.util.Log
import com.digipaytest.app.core.exception.Failure
import com.digipaytest.app.platform.NetworkHandler
import com.fernandocejas.sample.core.functional.Either
import com.moshaverOnline.app.core.base.BaseResponseModel
import retrofit2.Response

open class BaseRepository(private val networkHandler: NetworkHandler) {
    suspend fun <T, R> request(call: suspend()-> Response<BaseResponseModel<T>>, transform: (T) -> R, default: T): Either<Failure, R> {
        return try {
            if (networkHandler.isConnected == false) return Either.Left(Failure.NetworkConnection)
            val response = call.invoke()
            when (response.isSuccessful) {
                true -> Either.Right(transform((response.body()?.artists ?: default)))
                false -> {
                    Log.d("resCode", response.code().toString())
                    when (response.code()) {
                        401 -> Either.Left(Failure.TokenExpire)
                        else -> {
                            Either.Left(Failure.ServerError)
                        }
                    }
                }


            }

        } catch (exception: Throwable) {
            Log.d("EXEPTION",exception.message.toString())
            Either.Left(Failure.ServerError)
        }
    }


    suspend fun <T, R> simpleReturnRequest(call: suspend()-> Response<T>, transform: (T) -> R, default: T): Either<Failure, R> {
        return try {
            if (networkHandler.isConnected == false) return  Either.Left(Failure.NetworkConnection)
            val response = call.invoke()
            when (response.isSuccessful) {
                true -> Either.Right(transform((response.body() ?: default)))
                false -> Either.Left(if(response.body() != null) Failure.ServerMessageError(response.errorBody()?.string()?:"") else Failure.ServerError )
            }
        } catch (exception: Throwable) {
            Log.d("EXEPTION",exception.message.toString())
            Either.Left(Failure.ServerError)
        }
    }
}
