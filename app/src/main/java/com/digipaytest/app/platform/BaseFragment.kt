package com.digipaytest.app.platform

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.digipaytest.app.R
import com.digipaytest.app.core.base.BaseViewModel
import com.digipaytest.app.core.exception.Failure
import kotlinx.android.synthetic.main.fragment_base_layout.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.koin.android.viewmodel.ext.android.getSharedViewModel
import org.koin.android.viewmodel.ext.android.getViewModel
import java.lang.reflect.ParameterizedType
import kotlin.reflect.KClass


abstract class BaseFragment<M : BaseViewModel> : Fragment() {

    abstract fun layoutId(): Int
    open var toolbarTitle: String = ""
    abstract val toolbarLeftIMGResource: Int
    abstract val toolbarRightIMGResource: Int
    open fun rightItemClick() {}
    open fun leftItemClick(view: View) {}
    val viewModel: M by lazy { getSharedViewModel(viewModelClass()) }

    @Suppress("UNCHECKED_CAST")
    private fun viewModelClass(): KClass<M> {
        // dirty hack to get generic type https://stackoverflow.com/a/1901275/719212
        return ((javaClass.genericSuperclass as ParameterizedType)
            .actualTypeArguments[0] as Class<M>).kotlin
    }

    override fun onStart() {
        super.onStart()
        viewModel.failure.observe(this, Observer {

            when (it) {

                is Failure.ServerMessageError -> Toast.makeText(
                    activity,
                    it.message,
                    Toast.LENGTH_LONG
                ).show()
                is Failure.ServerError -> Toast.makeText(
                    activity,
                    R.string.error_connection_server,
                    Toast.LENGTH_LONG
                ).show()
                is Failure.NetworkConnection -> Toast.makeText(
                    activity,
                    getString(R.string.error_no_connection),
                    Toast.LENGTH_LONG
                ).show()
                is Failure.UserNotFound -> Toast.makeText(
                    activity,
                    getString(R.string.user_not_found),
                    Toast.LENGTH_LONG
                ).show()
//                is Failure.TokenExpire -> goToLoginPage()
            }
        })
    }

//    private fun goToLoginPage() {
//        startActivity(Intent(activity, LoginActivity::class.java).apply {
//            flags = Intent.FLAG_ACTIVITY_NEW_TASK
//        })
//    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_base_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val rootView = LayoutInflater.from(activity).inflate(layoutId(), layout_main, false)
        layout_main.addView(rootView)
        setUpToolbar()
        left_rel.setOnClickListener { leftItemClick(view) }
        right_rel.setOnClickListener { rightItemClick() }

        viewModel.progress.observe(this, Observer { t ->
            if (t) {
                progressBar.show()
//                layout_main.visibility = View.GONE

            } else {
                progressBar.hide()
                layout_main.visibility = View.VISIBLE
            }
        })
    }


    private fun setUpToolbar() {
        left_img.setImageResource(toolbarLeftIMGResource)
        right_img.setImageResource(toolbarRightIMGResource)
        center_txt.text = toolbarTitle
    }

    protected fun setToolbar(s: String) {
        left_img.setImageResource(toolbarLeftIMGResource)
        right_img.setImageResource(toolbarRightIMGResource)
        center_txt.text = s
    }

    protected fun hideToolbar() {
        toolbar.visibility = View.GONE
    }

    protected fun hideToolbarShadow() {
        toolbarShadow.visibility = View.GONE
    }
}

open class BaseFragment1<M : ViewModel> : Fragment() {
    val viewModel: M by lazy { getViewModel(viewModelClass()) }

    @Suppress("UNCHECKED_CAST")
    private fun viewModelClass(): KClass<M> {
        // dirty hack to get generic type https://stackoverflow.com/a/1901275/719212
        return ((javaClass.genericSuperclass as ParameterizedType)
            .actualTypeArguments[0] as Class<M>).kotlin
    }
}
