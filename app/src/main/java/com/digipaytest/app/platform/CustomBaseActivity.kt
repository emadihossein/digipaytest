package com.digipaytest.app.platform

import android.content.Intent
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.digipaytest.app.R
import com.digipaytest.app.core.base.BaseViewModel
import com.digipaytest.app.core.exception.Failure
import com.digipaytest.app.features.login.LoginActivity
import org.koin.android.viewmodel.ext.android.viewModel
import kotlin.reflect.KClass

abstract class CustomBaseActivity <M : BaseViewModel> (clazz: KClass<M>): AppCompatActivity()  {
    val viewModel: M by viewModel(clazz)

    override fun onStart() {
        super.onStart()
        viewModel.failure.observe(this, Observer {

            when (it) {

                is Failure.ServerMessageError -> Toast.makeText(
                    this,
                    it.message,
                    Toast.LENGTH_LONG
                ).show()
                is Failure.ServerError -> Toast.makeText(
                    this,
                    R.string.error_connection_server,
                    Toast.LENGTH_LONG
                ).show()
                is Failure.NetworkConnection -> Toast.makeText(
                    this,
                    getString(R.string.error_no_connection),
                    Toast.LENGTH_LONG
                ).show()
                is Failure.UserNotFound -> Toast.makeText(
                    this,
                    getString(R.string.user_not_found),
                    Toast.LENGTH_LONG
                ).show()
                is Failure.TokenExpire -> goToLoginPage()
            }
        })
    }

    private fun goToLoginPage() {
        startActivity(Intent(this, LoginActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        })
    }

}