buildscript {
    repositories {
        google()
        maven { url = uri( "https://jitpack.io" )}

        jcenter()
        maven { url = uri( "https://maven.google.com" )}
        maven {
            url  =uri ( "https://dl.bintray.com/kotlin/kotlin-dev/")
        }

    }
    dependencies {
        classpath (Plugins.android_gradle_plugin)
        classpath (Plugins.kotlin_gradle_plugin)
        classpath (Plugins.navigation_safe_arg_gradle_plugin)
        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }
}

allprojects {
    repositories {
        maven { url = uri( "https://maven.google.com" )}
        google()
        jcenter()
        maven { url = uri( "https://jitpack.io" )}

        maven {
            url  =uri ( "https://dl.bintray.com/kotlin/kotlin-dev/")
        }
    }
}

tasks.register("clean",Delete::class){
    delete(rootProject.buildDir)
}
